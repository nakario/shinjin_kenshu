from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = "".join(sys.stdin.readlines())
result = jumanpp.result(data)
mrph_trigram = zip(result.mrph_list(), result.mrph_list()[1:], result.mrph_list()[2:])
for a, b, c in mrph_trigram:
    if a.hinsi == "名詞" and b.genkei == "の" and c.hinsi == "名詞":
        print(a.genkei + b.genkei + c.genkei)
