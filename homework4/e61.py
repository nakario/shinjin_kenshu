import sys
from pyknp import Jumanpp

jmn = Jumanpp()

input_sentence = sys.stdin.readline()
result = jmn.analysis(input_sentence)
print(" ".join(mrph.midasi for mrph in result.mrph_list()))
