from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = "".join(sys.stdin.readlines())
result = jumanpp.result(data)
print(len(list(filter(lambda m: m.hinsi in ["動詞", "形容詞"], result.mrph_list()))) / len(list(result.mrph_list())))
