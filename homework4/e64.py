from pyknp import Jumanpp
import sys
from collections import defaultdict

jumanpp = Jumanpp()

data = "".join(sys.stdin.readlines())
result = jumanpp.result(data)
d = defaultdict(int)
for mrph in result.mrph_list():
    d[mrph.genkei] += 1
for word, cnt in sorted(d.items(), key=lambda x: x[1], reverse=True):
    print(word, cnt)
