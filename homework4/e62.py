from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = "".join(sys.stdin.readlines())
result = jumanpp.result(data)
print("\n".join(mrph.midasi for mrph in result.mrph_list() if mrph.hinsi == "名詞"))
