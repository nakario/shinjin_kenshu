from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = "".join(sys.stdin.readlines())
result = jumanpp.result(data)
mrph_bigram = zip(result.mrph_list(), result.mrph_list()[1:])
for a, b in mrph_bigram:
    if a.bunrui == "サ変名詞" and b.genkei in ["する", "できる"]:
        print(a.genkei + b.genkei)
