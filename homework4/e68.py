import sys
from pyknp import KNP

knp = KNP(jumanpp=True)

input_sentence = sys.stdin.readline()
result = knp.parse(input_sentence)
print(" ".join("".join(mrph.midasi for mrph in bnst.mrph_list()) for bnst in result.bnst_list()))
