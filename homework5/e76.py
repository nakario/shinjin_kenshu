from pyknp import KNP 

knp = KNP()
result = knp.parse("望遠鏡で泳いでいる美しい少女を見た。")

#文節のリスト
for bnst in result.bnst_list():
    parent = bnst.parent
    if parent is not None:
        child_rep = " ".join(mrph.repname for mrph in bnst.mrph_list()).strip()
        parent_rep = " ".join(mrph.repname for mrph in parent.mrph_list()).strip()
        if any(mrph.hinsi in ["動詞", "形容詞"] for mrph in bnst.mrph_list()) and\
           any(mrph.hinsi == "名詞" for mrph in parent.mrph_list()) and\
           abs(parent.bnst_id - bnst.bnst_id) > 1:
            print(child_rep, "->", parent_rep, abs(parent.bnst_id - bnst.bnst_id) -1)
