import sys
from pyknp import KNP

knp = KNP(jumanpp=True)

data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            if any(mrph.hinsi == "名詞" for mrph in bnst.mrph_list()) and\
               any(mrph.hinsi == "接尾辞" for mrph in bnst.mrph_list()):
                print("".join(mrph.midasi for mrph in bnst.mrph_list()))
        data = ""
