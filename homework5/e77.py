from pyknp import KNP 

knp = KNP()
result = knp.parse("僕等の明日の光の先の瞳の奥の 大空の下の夢の扉の輝きの 涙の力の心のカギの永久の自由の 果ての約束の手の油 ギャグマンガ日和")

#文節のリスト
mlwi = [] # mrph list with index
for i, bnst in enumerate(result.bnst_list()):
    for mrph in bnst.mrph_list():
        mlwi.append((mrph, i))
trigram = zip(mlwi, mlwi[1:], mlwi[2:])
for x, y, z in trigram:
    if x[0].hinsi == "名詞" and y[0].midasi == "の" and z[0].hinsi == "名詞"\
       and result.bnst_list()[x[1]].parent is not None\
       and result.bnst_list()[x[1]].parent.bnst_id\
       != result.bnst_list()[z[1]].bnst_id:
        start = result.bnst_list()[x[1]].bnst_id # Aの文節
        end = result.bnst_list()[x[1]].parent.bnst_id # Aが係る文節
        for bnst in result.bnst_list():
            if start <= bnst.bnst_id <= end:
                print("".join(mrph.midasi for mrph in bnst.mrph_list()).strip(), end=" ")
        print()
