print("e4")
a = "Yakult"
b = "Swallows"
c = a[0] + b[0]
print(c)

print("e5")
a = "yakultswallows"
b = []
c = 0
for i in sorted(a):
    c -= 1
    if c > 0:
        continue
    c = a.count(i)
    print(i, c)

print("e6")
a = "stressed"
print(a[::-1])

print("e7")
a = "パタトクカシーー"
b = a[::2]
print(b)

print("e8")
a = "パトカー"
b = "タクシー"
c = ""
for (i, j) in zip(a, b):
    c += i + j
print(c)

print("e9")
a = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."
b = ""
for i, v in enumerate(a.split()):
    if i in [1, 5, 6, 7, 8, 9, 15, 16, 19]:
        b += v[0]
    else:
        b += v[:2]
print(b)
