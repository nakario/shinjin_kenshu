import re

def doubleVowelsWithRegex(s):
    vowels = re.compile(r"([aeiou])")
    return vowels.sub(r"\1\1", s)

def doubleVowelsWithoutRegex(s):
    return "".join(map(lambda c: c*2 if c in "aeiou" else c, list(s)))

s = "Sir Robin"
print(f"original: {s}")
print(f"with regex: {doubleVowelsWithRegex(s)}")
print(f"without regex: {doubleVowelsWithoutRegex(s)}")
