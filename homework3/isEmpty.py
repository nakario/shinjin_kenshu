import re

def isEmptyWithRegex(s):
    return re.match(r"[\s\S]+", s) == None

def isEmptyWithoutRegex(s):
    return s == ""

s = ""
assert isEmptyWithRegex(s) == True
assert isEmptyWithoutRegex(s) == True

for s in [" ", "\n", "hoge", "123", "あ"]:
    assert isEmptyWithRegex(s) == False
    assert isEmptyWithoutRegex(s) == False
