import sys

with open(sys.argv[1]) as utf8File:
    with open('split.txt', 'w') as out:
        for line in utf8File:
            for word in line.split(' '):
                out.write(word + '\n')
