import gzip
from collections import defaultdict

def estimate(sentence, words):
    bigrams = list(zip(words, words[1:]))
    swords = sentence.split(" ")
    sbigrams = list(zip(swords, swords[1:]))
    dwords = defaultdict(int)
    dbigrams = defaultdict(int)
    for word in words:
        dwords[word] += 1
    for bigram in bigrams:
        dbigrams[bigram] += 1
    p = dwords[swords[0]] / len(words)
    for b, w in zip(sbigrams, swords):
        p *= dbigrams[b] / dwords[w]
    return p

words = []
with gzip.open("/share/text/WWW.en/txt.en/tsubame00/doc0000000000.txt.gz", "rt") as doc:
    for line in doc:
        if line.startswith("<PAGE URL=") or line.startswith("</PAGE>"):
            continue
        for word in line.split(" "):
            words.append(word.strip())

print(estimate("The man is in the house.", words))
