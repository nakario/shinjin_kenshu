import sys

if len(sys.argv) < 3:
    sys.stderr.write('usage: python3 cp.py src dst1 dst2 ...')
    exit()

for dstPath in sys.argv[2:]:
    with open(dstPath, 'w') as dst:
        with open(sys.argv[1]) as src:
            for line in src:
                dst.write(line)
