print("Homework 1")
for i in range(1, 31):
    print(i)

print("Homework 2")
for i in range(1, 31):
    if i % 2 == 0:
        print(i)

print("Homework 3")
sum = 0
for i in range(1, 31):
    if i % 2 == 0:
        sum += i
print(sum)

print("Homework 4")
factorial = 1
for i in range(1, 11):
    factorial *= i
print(factorial)

print("Homework 5")
for i in range(1, 10):
    for j in range(1, 10):
        print("%2d " % (i * j), end="")
    print()

print("Homework 6")
for i in range(1, 31):
    if i % 3 == 0 and i % 5 == 0:
        print("FizzBuzz")
    elif i % 3 == 0:
        print("Fizz")
    elif i % 5 == 0:
        print("Buzz")
    else:
        print(i)

print("Homework 7")
for i in range(2, 1001):
    isPrime = True
    for j in range(2, i):
        if i % j == 0:
            isPrime = False
            break
    if isPrime:
        print(i)
