# Calculate 5 to the power of 4
print(5 ** 4)

# Calculate 1 divided by 4
print(1 / 4)

# Causes ZeroDivisionError
1 / 0
