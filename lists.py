import sys

# e1
a = [1, 2, 3]
b = []
for i in a:
    b.append(i)
print(a, b)

# e2
evens = list(range(2, 31, 2))
for i, v in enumerate(evens):
    if v % 3 == 0:
        evens[i] = 0
print(evens)

# e3
days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
i = int(sys.argv[1])
if i in range(1, 8):
    print(days[i-1])
