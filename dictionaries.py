import sys
import random
import string

print("e10")
a = ['eggs', 'spam', 'spam', 'bacon']
b = dict(zip(a,[a.count(i) for i in a]))
print(b)

print("e11")
# usage: python3 dictionaries.py
#        python3 dictionaries.py (string)
#        python3 dictionaries.py (seed) (string)
seed = 0 if len(sys.argv) < 3 else sys.argv[1]
random.seed(seed)
a = string.ascii_lowercase
b = "".join(random.sample(a, len(a)))
encrypter = dict(zip(a, b))
decrypter = dict(zip(b, a))
c = "hoge"
if len(sys.argv) == 2:
    c = sys.argv[1]
elif len(sys.argv) > 2:
    c = sys.argv[2]
d = "".join([encrypter[i] for i in c])
e = "".join([decrypter[i] for i in d])
print(f"raw: {c}")
print(f"encrypted: {d}")
print(f"decrypted: {e}")
