# Homework 2
def max_with_index(a):
    max = sorted(a)[::-1][0]
    return max, a.index(max)

a = [1, 3, -1, 5, 2, 5]
max, index = max_with_index(a)
print(a)
print(f"max: {max}, index: {index}")
