# Homework 4-2
import sys

def fibonacci2(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

n = int(sys.argv[1])
print(fibonacci2(n))
