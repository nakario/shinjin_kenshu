# Homework 5
def gcd(a, b):
    if b == 0:
        return a
    return gcd(b, a % b)

a, b = 1071, 1029
print(f"a: {a}, b: {b}, gcd(a, b): {gcd(a, b)}")
