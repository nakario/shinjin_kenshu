# Homework 6
import math
import time

def eratosthenes(n):
    a = list(range(2, n + 1))
    primes = []
    while True:
        i = a.pop(0)
        primes.append(i)
        a = list(filter(lambda x: x % i != 0, a))
        if i > math.sqrt(n): break
    return primes + a

def primes(n):
    primes = []
    for i in range(2, n + 1):
        isPrime = True
        for j in range(2, i):
            if i % j == 0:
                isPrime = False
                break
        if isPrime:
            primes.append(i)
    return primes

t0 = time.clock()
eratosthenes(10000)
t1 = time.clock()
print(f"Eratosthenes: dt = {str(t1 - t0)} [s]")

t2 = time.clock()
primes(10000)
t3 = time.clock()
print(f"Simple implementation: dt = {str(t3 - t2)} [s]")
