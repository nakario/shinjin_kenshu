# Homework 7

# バブルソート
def bubble_sort(lst):
    ret = lst[:]
    for i in range(len(ret) - 1):
        for j in range(len(ret) - 1 - i):
            if ret[j] > ret[j + 1]:
                ret[j], ret[j + 1] = ret[j + 1], ret[j]
    return ret

# クイックソート
# 配列内に重複する要素は無いと仮定
def quick_sort(lst):
    if len(lst) < 2: return lst
    pivot = lst[len(lst) // 2]
    lower = list(filter(lambda x: x < pivot, lst))
    higher = list(filter(lambda x: x > pivot, lst))
    return quick_sort(lower) + [pivot] + quick_sort(higher)

# マージソート
def merge_sort(lst):
    if len(lst) < 2: return lst
    left = merge_sort(lst[:len(lst) // 2])
    right = merge_sort(lst[len(lst) // 2:])
    ret = []
    while len(left) > 0 and len(right) > 0:
        if left[0] < right[0]:
            ret.append(left.pop(0))
        else:
            ret.append(right.pop(0))
    ret += left + right
    return ret

a = [1, 4, 2, 6, 7, 3]
print(a)
print(bubble_sort(a))
print(quick_sort(a))
print(quick_sort(a))
