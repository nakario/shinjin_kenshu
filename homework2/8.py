# Homework 8
from itertools import product
from copy import deepcopy

# (i, j)にクイーンを置いた時に一手で移動できるマスを1、それ以外を0
# とするn*n行列を返す
def reachableArea(n, i, j):
    area = [[0 for _ in range(n)] for _ in range(n)]
    for i_, j_ in product(range(n), range(n)):
            if i_ == i or j_ == j or abs(i_ - i) == abs(j_ - j):
                area[i_][j_] = 1
    return area

# n-queenパズルを解く
# board: n*n行列、0なら新たにクイーンを設置可能、
#        1以上なら既に他のクイーンの移動範囲
# queens: 各行のクイーンがどの列にいるか
# i: 今何行目について考えているか
def solveQueens(n, board=None, queens=None, i=0):
    b = deepcopy(board) if board != None else\
        [[0 for _ in range(n)] for _ in range(n)]
    qs = queens[:] if queens != None else\
        [0 for _ in range(n)]
    ans = []
    for j in range(n):
        if b[i][j] == 0:
            area = reachableArea(n, i, j)
            for k, l in product(range(n), range(n)):
                b[k][l] += area[k][l]
            qs[i] = j
            if i == n - 1:
                ans.append(qs)
            else:
                ans += solveQueens(n, b, qs, i + 1)
            for k, l in product(range(n), range(n)):
                b[k][l] -= area[k][l]
    return ans

answers = solveQueens(8)
print(f"{len(answers)} answers")
for ans in answers:
    print(ans)
