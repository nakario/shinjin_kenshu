# Homework 1
def max(a, b, c):
    return sorted([a, b, c])[::-1][0]

a, b, c = 2, -1, 5
print(f"a: {a}, b: {b}, c: {c}, max(a, b, c): {max(a, b, c)}")
