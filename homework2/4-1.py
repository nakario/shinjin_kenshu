# Homework 4-1
import sys

def fibonacci1(n):
    if n < 3:
        return 1
    return fibonacci1(n - 1) + fibonacci1(n - 2)

n = int(sys.argv[1])
print(fibonacci1(n))
