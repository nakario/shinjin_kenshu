# モジュール一式のimport
import numpy as np
import chainer
from chainer import cuda, Function, gradient_check, report, training, utils, Variable
from chainer import datasets, iterators, optimizers, serializers
from chainer import Link, Chain, ChainList
import chainer.functions as F
import chainer.links as L
from chainer.training import extensions
from chainer.datasets import mnist

# データセットがダウンロード済みでなければ、ダウンロードも行う
train, test = mnist.get_mnist(withlabel=True, ndim=1)

batchsize = 128 # minibatchのサイズ

# デフォルトでデータセット内の事例をshuffleする
train_iter = iterators.SerialIterator(train, batchsize)
test_iter = iterators.SerialIterator(test, batchsize,
                                     repeat=False, shuffle=False)

class CNN(chainer.Chain):

    def __init__(self, c1=16, c2=32, c3=64, k=3, n_out=10):
        # パラメータを持つ層の登録
        super(MLP, self).__init__(
            # Linear は W x + b の線形変換 (全結合) を行うLink
            # 第1引数は入力の次元数 (Noneの場合は、初回呼び出し時にデータから自動的に推測)
            # 第2引数は出力の次元数
            conv1=L.Convolution2D(1, c1, k),
            conv2=L.Convolution2D(c1, c2, k),
            conv3=L.Convolution2D(c2, c3, k),
            l1=L.Linear(None, n_out),
        )

    def __call__(self, x):
        # データを受け取った際のforward計算を書く
        # relu は Rectified Linear Unit: f(x)=max(0,x)
        x = x.reshape((len(x) // 784, 1, 28, 28))
        h = F.relu(self.conv1(x))
        h = F.max_pooling_2d(h, 2)
        h = F.relu(self.conv2(h))
        h = F.max_pooling_2d(h, 2)
        h = F.relu(self.conv3(h))
        h = F.max_pooling_2d(h, 2)
        return self.l1(h)

# gpu_id = 0

model = CNN()
# model.to_gpu(gpu_id)

print('1つ目の全結合相のバイアスパラメータの形は、', model.l1.b.shape)
print('初期化直後のその値は、', model.l1.b.data)

optimizer = optimizers.SGD(lr=0.01)
optimizer.setup(model)

from chainer.dataset import concat_examples
from chainer.cuda import to_cpu

max_epoch = 10

while train_iter.epoch < max_epoch:
    
    # ---------- 学習の1イテレーション ----------
    train_batch = train_iter.next()
    # minibatch の事例列を連結して配列にします
    x, t = concat_examples(train_batch)
    x = Variable(x) # (128, 784)
    t = Variable(t) # (128,)
    
    # 予測値の計算
    y = model(x) # (128, 10)

    # ロスの計算
    loss = F.softmax_cross_entropy(y, t)

    # 勾配の計算
    model.cleargrads()
    loss.backward()

    # パラメータの更新
    optimizer.update()
    # --------------- ここまで ----------------

    # 1エポック終了ごとにValidationデータに対する予測精度を測って、
    # モデルの汎化性能が向上していることをチェックしよう
    if train_iter.is_new_epoch:  # 1 epochが終わったら

        # ロスの表示
        print('epoch:{:02d} train_loss:{:.04f} '.format(
            train_iter.epoch, float(to_cpu(loss.data))), end='')

        test_losses = []
        test_accuracies = []
        while True:
            test_batch = test_iter.next()
            x_test, t_test = concat_examples(test_batch)
            x_test = Variable(x_test)
            t_test = Variable(t_test)

            # テストデータをforward
            y_test = model(x_test)

            # ロスを計算
            loss_test = F.softmax_cross_entropy(y_test, t_test)
            test_losses.append(to_cpu(loss_test.data))

            # 精度を計算
            accuracy = F.accuracy(y_test, t_test)
            accuracy.to_cpu()
            test_accuracies.append(accuracy.data)
            
            if test_iter.is_new_epoch:
                test_iter.epoch = 0
                test_iter.current_position = 0
                test_iter.is_new_epoch = False
                test_iter._pushed_position = None
                break

        print('val_loss:{:.04f} val_accuracy:{:.04f}'.format(
            np.mean(test_losses), np.mean(test_accuracies)))

from chainer import serializers

serializers.save_npz('my_mnist.model', model)

# まず同じモデルのオブジェクトを作る
infer_model = MLP()

# そのオブジェクトに保存済みパラメータをロードする
serializers.load_npz('my_mnist.model', infer_model)

# GPU上で計算させるために、モデルをGPUに送る
# infer_model.to_gpu(gpu_id)

x, t = test[0]
print('label:', t)

# ミニバッチの形にする（ここではサイズ1のミニバッチにするが、
# 複数まとめてサイズnのミニバッチにしてまとめて推論することもできる）
print(x.shape, end=' -> ')
x = x[None, ...]
print(x.shape)

# モデルのforward関数に渡す
y = infer_model(x)

# Variable形式で出てくるので中身を取り出す
y_data = y.data

# 最大値のインデックスを見る
pred_label = y_data.argmax(axis=1)

print('predicted label:', pred_label[0])
